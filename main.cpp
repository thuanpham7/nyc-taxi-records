#include "boost/date_time/posix_time/posix_time.hpp"
#include <fstream>
#include <iostream>
#include <map>
#include <sstream>
#include <string>
using namespace std;
using namespace boost::posix_time;

int main(int argc, char **argv) {

	map <int, map<string, double> > myMap; 		// -Create a map that store total distance match year, weekday (int year, string weekday, double distance)
	map <int, map<string, int> > count;		// -Create a map that track the sequence of total time travel (int time)

	for(int i = 1; i < argc; i++) {
		ifstream f(argv[i]);
		if(f.is_open()) {
			cout << "Reading file: " << argv[i] << endl;
			string line, s1, s2, s5;
			double thedistance;
			while(getline(f, line)) {
				istringstream ss(line);	//-Create istringstream to copy the string from each value gain
				getline(ss, s1, ',');   //-store value from the specific col since comma that get to the next column
				getline(ss, s2, ',');

				//-Convert string s2 to string of date and time
				string timestr = s2;
				ptime t = time_from_string(timestr);
				int year = t.date().year();
				string weekday = boost::lexical_cast<std::string>(t.date().day_of_week());

				getline(ss, s1, ',');
				getline(ss, s1, ',');
				getline(ss, s5, ',');
				thedistance = stod(s5);		//-Convert string s5(col 5) to double

				if (myMap.count(year)){         
					if (myMap[year].count(weekday)){  
						myMap[year][weekday] = myMap[year][weekday] + thedistance;  //-if the key is found, add up the distance
						count[year][weekday] +=1;  //-Add up number of flight that date of year
					}else{
						myMap[year][weekday] = thedistance;  //-Set up the distance to the key of year and date
						count[year][weekday] = 1;	     //-Set up the total travels that date of year
					}
				}else{	//-Set key and value if the key of the map is empty of year
					myMap[year][weekday] = thedistance;
					count[year][weekday] = 1;
				}
			}
			f.close();
		}
	}

	//-Create iterators that point to the maps
	map<int, map<string, double > >::const_iterator it;
	map<string, double>::const_iterator another;   

	//-Print values
	for(it = myMap.begin(); it !=myMap.end(); it++) {
		for(another = it->second.begin(); another != it->second.end(); another++){
			double average = another->second/count[it->first][another->first];  //-Calculate the average of distance travel each date of year
			cout << it->first << " ";
			cout << another->first << ": count= " << count[it->first][another->first] << ", avg miles = " << average << endl;
		}

	}

	return 0;
}
